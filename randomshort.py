
import string
from urllib import parse
from webapp import WebApp
import random
import shelve

content = shelve.open('content')

PRINCIPAL_PAGE = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Simply url shortener website </title>
    </head>
        <div>
            <font color="#A40808" size="20" align="center"> Url shortener website</font>
        </div>
        <div>
           <p> 
            <font size="5" align="center"> URL LIST: <br><br>{url_list} </font>
           </p>
        </div>
        <div>
            {form}
        </div>
    </body>
</html>
"""

FORM = """
<hr>
    <form action="/" method="post">
      <div>
        <label> 
            <font size="5" align="center"> URL: </font>
        </label>
        <input type="text" name="url" required>
      </div>
        <input type="submit" value="Send">
      </div>
    </form>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Simply url shortener website </title>
    </head>
        <div> 
            <font size="5" align="center" > Resource: {resource} Not available. </font>
        </div>
        <div>
            {form}
        </div>
    </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Simply url shortener website </title>
    </head>
        <h1>
            <font size="5" align="center"> Unprocessable, something wrong: {body} </font>
        </h1>
    </body>
</html>
"""

PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Simply url shortener website </title>
    </head>
        <div>
            <font color="#A40808" size="20" align="center"> Url shortener website</font>
        </div>
        <div>
           <p> 
            <font size="5" align="center"> URL LIST:<br><br> {url_list} </font>
           </p>
        </div>
        <div>
            {form}
        </div>
    </body>
</html>
"""

NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Simply url shortener website </title>
    </head>
        <h1>
            <font size="5" align="center"> Method not allowed: {method} </font>
        </h1>
    </body>
</html>
"""

class RandomShort(WebApp):

    def print_dictionary(self):
        dictionary = ""
        for element in content:
            dictionary = dictionary + "Long Url: " + content[element] + "<br>" + " Short Url: localhost:1234" + element + "<br>"
        return dictionary

    def parse(self, request):
        data = {}
        start = request.find('\r\n\r\n')
        if start == -1:
            data['body'] = None
        else:
            data['body'] = request[start + 4:]
        request_parts = request.split(' ', 2)
        data['method'] = request_parts[0]
        data['resource'] = request_parts[1]
        return data

    def process(self, data):
        if data['method'] == 'GET':
            code, value = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, value = self.post(data['resource'], data['body'])
        else:
            value = NOT_ALLOWED.format(method='POST')
            code = "405 Method not allowed"
        return code, value

    def get(self, resource):
        if resource == '/':
            value = PRINCIPAL_PAGE.format(url_list=self.print_dictionary(), form=FORM)
            code = "200 OK"
        elif resource in content:
            url = content[resource]
            value = ""
            code = "301 Moved Permanently\r\nLocation:" + url
        else:
            value = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
            code = "404 NOT FOUND"
        return code, value

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        print(fields)
        if resource == '/':
            if 'url' in fields:
                url = self.complete_url(fields['url'][0])
                if url in content.values():
                    shorten_url = list(content.keys())
                    value = PAGE_POST.format(url_list=self.print_dictionary(), form=FORM)
                    code = "200 OK"
                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = '/' + random_key
                    content[shorten_url] = url
                    print(shorten_url)
                    value = PAGE_POST.format(url_list=self.print_dictionary(), form=FORM)
                    code = "200 OK"
            else:
                value = PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            value = PAGE_UNPROCESSABLE.format(body=body)
            code = "422 Unprocessable Entity"
        return code, value

    def complete_url(self, url):
        if "http://" in url or "https://" in url:
            return url
        else:
            return "http://" + url


if __name__ == "__main__":
    try:
        server = RandomShort('localhost', 1234)
    except KeyboardInterrupt:
        content.close()
        print("Connexion Finished")