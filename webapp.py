import socket

class WebApp:
    def __init__ (self, hostname, port):

        #se crea el socket
        my_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.bind((hostname, port))

        #Maximo de 5 conexiones simultáneas.
        my_socket.listen(5)

        while True:
            print("Serving at port", port)
            print("Waiting for connections...")
            (recvSocket, address) = my_socket.accept()
            request = recvSocket.recv(2048)
            parsedRequest = self.parse(request.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(parsedRequest)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                        + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()